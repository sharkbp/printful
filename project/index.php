<?php
class DBconnect {
	private $con;
        function __construct() {
        $this->con = $this->connectDB();
	}	
	function connectDB() {
		$con = mysqli_connect("mysql","root","secret","tasks");
		return $con;
	}
        function runQuery($query) {
                $result = mysqli_query($this->con,$query);
                while($row=mysqli_fetch_assoc($result)) {
                $resultset[] = $row;
                }		
                if(!empty($resultset))
                return $resultset;
	}
}

$db_handle = new DBconnect();
$PersonResult = $db_handle->runQuery("SELECT DISTINCT CONCAT(p.name,' ',p.surname) as person FROM people p ORDER BY person");
?>
<!DOCTYPE html>
<html>
      
<head>
    <title>
        PHP API
        to process JSON files and display tasks from MySQL
    </title>
</head>
  
<body style="text-align:center;">
    <h1 style="color:green;">
        Pavel Baranchuk test task1
    </h1>
 
    <h2>Select Person from the list to display tasks</h2>
    <form method="POST" name="search" action="index.php">
        <div id="persons-grid">
            <div class="search-box">
                <select id="person" name="person[]" multiple="multiple">
                    <option value="0" selected="selected">Select Person</option>
                        <?php
                        if (! empty($PersonResult)) {
                            foreach ($PersonResult as $key => $value) {
                                echo '<option value="' . $PersonResult[$key]['person'] . '">' . $PersonResult[$key]['person'] . '</option>';
                            }
                        }
                        ?>
                </select><br> <br>
                <button id="Filter">Search</button>
            </div>
            
                <?php
                if (! empty($_POST['person'])) {
                    ?>
                    <table cellpadding="10" cellspacing="1">

                <thead>
                    <tr>
                        <th><strong>id</strong></th>
                        <th><strong>person</strong></th>
                        <th><strong>title</strong></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $query = "SELECT p.id,CONCAT(p.name,' ',p.surname) as person,t.title FROM people p JOIN tasks t ON p.id=t.person_id";
                    $i = 0;
                    $selectedOptionCount = count($_POST['person']);
                    $selectedOption = "";
                    while ($i < $selectedOptionCount) {
                        $selectedOption = $selectedOption . "'" . $_POST['person'][$i] . "'";
                        if ($i < $selectedOptionCount - 1) {
                            $selectedOption = $selectedOption . ", ";
                        }
                        
                        $i ++;
                    }
                    $query = $query . " WHERE CONCAT(p.name,' ',p.surname) in (" . $selectedOption . ")";
                    $result = $db_handle->runQuery($query);
                }
                if (! empty($result)) {
                    foreach ($result as $key => $value) {
                        ?>
                <tr>
                        <td><div class="col" id="user_data_1"><?php echo $result[$key]['id']; ?></div></td>
                        <td><div class="col" id="user_data_3"><?php echo $result[$key]['person']; ?> </div></td>
                        <td><div class="col" id="user_data_2"><?php echo $result[$key]['title']; ?> </div></td>
                    </tr>
                <?php
                    }
                    ?>
                    
                </tbody>
            </table>
            <?php
                }
                ?>  
        </div>
    </form>
          
    <h4>
        PHP API
        to process JSON files and display tasks from MySQL
    </h4>
      
    <?php
        require_once('index.php');
        if(array_key_exists('process_json', $_POST)) {
            process_json();
        }
        else if(array_key_exists('dispay_tasks', $_POST)) {
            dispay_tasks();
        }
        else if(array_key_exists('truncate_table', $_POST)) {
            truncate_table();
        }
        else if(array_key_exists('create_tables', $_POST)) {
                create_tables();
        }        
        function process_json() {
            //connect to mysql db
            $con = mysqli_connect("mysql","root","secret","tasks");
            // Check connection
            if (!$con) {
              die("Connection failed: " . mysqli_connect_error());
            }

            //read file and convert json object to php associative array
            $content = json_decode(file_get_contents('data.json'), true);
        
            //get the tasks array
            foreach ($content as $data) {
                $title = $data['title'];
                $person = preg_split('/\s+/', $data['person']);
                $person_name = trim($person[0]);
                $person_surname = trim($person[1]);
            
                //insert into mysql table
                $sql = "INSERT INTO tasks (title, person_id) SELECT '$title', id FROM people WHERE name = '$person_name' AND surname = '$person_surname';";
                if (mysqli_query($con, $sql)) {
                  } else {
                    echo "Error: " . $sql . PHP_EOL . mysqli_error($con);
                  }
            }
            mysqli_close($con);
            echo 'Data from JSON file was loaded';
            require('tasks.php'); 
        }

        function dispay_tasks() {
            require_once('tasks.php');
        }

        function create_tables() {
            $con = mysqli_connect("mysql","root","secret");

            $query = '';
            $sqlScript = file('/usr/share/nginx/html/schema.sql');
            foreach ($sqlScript as $line)	{
                
                $startWith = substr(trim($line), 0 ,2);
                $endWith = substr(trim($line), -1 ,1);
                
                if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
                    continue;
                }
                    
                $query = $query . $line;
                if ($endWith == ';') {
                    mysqli_query($con,$query) or die('<div class="error-response sql-import-response">Problem in executing the SQL query <b>' . $query. '</b></div>');
                    $query= '';		
                }
            }
            echo '<div class="success-response sql-import-response">SQL file imported successfully</div>';
        }
                
        function truncate_table() {
            //connect to mysql db
            $con = mysqli_connect("mysql","root","secret","tasks");
            // Check connection
            if (!$con) {
              die("Connection failed: " . mysqli_connect_error());
            }

            //read file and convert json object to php associative array
            $content = json_decode(file_get_contents('data.json'), true);
        
            //get the tasks array
            foreach ($content as $data) {
                $title = $data['title'];
                $person = preg_split('/\s+/', $data['person']);
                $person_name = trim($person[0]);
                $person_surname = trim($person[1]);
            
                //insert into mysql table
                $sql = "truncate tasks;";
                if (mysqli_query($con, $sql)) {
                  } else {
                    echo "Error: " . $sql . PHP_EOL . mysqli_error($con);
                  }
            }
            mysqli_close($con);
            echo 'Data were removed from the DATABASE';
            require('tasks.php'); 
        }
    ?>
  
    <form method="post">
        <input type="submit" name="process_json"
                class="button" value="Process JSON" />
          
        <input type="submit" name="dispay_tasks"
                class="button" value="Dispay tasks" />
        
        <input type="submit" name="truncate_table"
                class="button" value="Remove tasks" />

        <input type="submit" name="create_tables"
            class="button" value="Create tables from SQL dump" />         
    </form>
</head>
  
</html>
