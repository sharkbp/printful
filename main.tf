terraform {
  required_version = "~> 1.0.9"

  backend "s3" {
    bucket = "custom-bucket-name"
    key    = "remote_state"
    region = "eu-west-1"
  }
}

provider "aws" {
  version = ">= 2.28.1"
  region  = var.region
}

provider "local" {
  version = "~> 1.2"
}

provider "null" {
  version = "~> 2.1"
}

provider "template" {
  version = "~> 2.1"
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.11"
}

provider "random" {
  version = "~> 2.1"
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_availability_zones" "available" {
}

locals {
  cluster_name = "ephemeral-eks-${var.cluster_prefix_name}"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "1.1.0"

  name = local.cluster_name

  cidr = "10.0.0.0/16"

  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]

  enable_nat_gateway    = true
  single_nat_gateway    = true
  enable_dns_hostnames  = true

  azs             = data.aws_availability_zones.available.names

  vpc_tags = {
    Name = local.cluster_name
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "1.1.0"

  cluster_name    = local.cluster_name
  cluster_version = "1.20"

  subnets      = module.vpc.public_subnets
  vpc_id       = module.vpc.vpc_id

  node_groups = {
    node-group = {
      name             = local.cluster_name
      desired_capacity = 3
      max_capacity     = 5
      min_capacity     = 1

      instance_type = "t2.micro"
      k8s_labels = {
        Environment = "ephemeral"
      }
    }
  }
}

resource "aws_ecr_repository" "default" {
  name  = "my-app"
}

data "aws_elb_hosted_zone_id" "main" {}

resource "aws_route53_record" "www" {
  count   = length(var.aws_elb_hostname)>1 ? 1 : 0
  zone_id = var.aws_zone_id
  name    = var.route53_record_name
  type    = "A"

  alias {
    name                   = var.aws_elb_hostname
    zone_id                = data.aws_elb_hosted_zone_id.main.id
    evaluate_target_health = true
  }
}
